﻿using Xunit;

namespace Fortu.Domain.UnitTests
{
    public class DefaultErrorTests
    {
        public class ExampleError : DefaultError { }

        [Fact]
        public void Message_Should_ReturnDerivedTypeFullName()
        {
            var result = new ExampleError();

            Assert.Equal("Fortu.Domain.UnitTests.DefaultErrorTests+ExampleError", result.Message);
        }
    }
}

﻿using Feree.ResultType.Results;
using Fortu.Domain.Accounts.Models;
using Xunit;

namespace Fortu.Domain.UnitTests
{
    public class CredentialsTests
    {
        [Fact]
        public void Create_Should_ReturnSuccess_When_ValidDataGiven()
        {
            var result = Credentials.Create("validPassword");

            Assert.True(result is Success);
        }

        [Fact]
        public void Create_Should_ReturnFailure_When_TooShortPassword()
        {
            var result = Credentials.Create("to_short");

            Assert.True(result is Failure);
        }

        [Fact]
        public void Create_Should_ReturnFailure_When_TooLongPassword()
        {
            var result = Credentials.Create(
                "qwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfghqwertyuiopasdfgh");

            Assert.True(result is Failure);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Media.Errors;

namespace Fortu.Domain.Media.Models
{
    public class Media
    {
        public Guid Id { get; }
        public string Type { get;  }
        public string Url { get; }
        public string OwnerId { get; }
        public IEnumerable<string> ReceiversIds { get; }

        private Media(Guid id, string type, string url, string ownerId, IEnumerable<string> receiversIds)
        {
            Id = id;
            Type = type;
            OwnerId = ownerId;
            Url = url;
            ReceiversIds = receiversIds;
        }

        public static IResult<Media> Reconstitute(Guid id, string type, string url, string ownerId, IEnumerable<string> receiversIds)
            => Validate(id, type, url, ownerId, receiversIds)
                .Bind(_ => new Media(id, type, url, ownerId, receiversIds).AsResult());

        public static IResult<Media> Create(string type, string url, string ownerId, IEnumerable<string> receiversIds)
            => Reconstitute(Guid.NewGuid(), type, url, ownerId, receiversIds);

        private static IResult<Unit> Validate(Guid id, string type, string url, string ownerId, IEnumerable<string> receivers)
        {
            if(id == Guid.Empty)
                return ResultFactory.CreateFailure(new MediaErrors.IdIsRequiredError());

            if (!string.IsNullOrEmpty(type))
                return ResultFactory.CreateFailure(new MediaErrors.TypeIsRequiredError());

            if (!string.IsNullOrEmpty(url))
                return ResultFactory.CreateFailure(new MediaErrors.UrlIsRequiredError());

            if (!string.IsNullOrEmpty(ownerId))
                return ResultFactory.CreateFailure(new MediaErrors.OwnerIdIsRequiredError());

            if (receivers is null)
                return ResultFactory.CreateFailure(new MediaErrors.ReceiversIdsAreRequiredError());

            if (!receivers.Any())
                return ResultFactory.CreateFailure(new MediaErrors.NotEnoughReceiversError());

            return ResultFactory.CreateSuccess();
        }
    }
}

﻿using Feree.ResultType.Results;

namespace Fortu.Domain.Media.Errors
{
    public static class MediaErrors
    {
        public class IdIsRequiredError : IError
        {
            public string Message => "id is required";
        }

        public class TypeIsRequiredError : IError
        {
            public string Message => "type is required";
        }

        public class UrlIsRequiredError : IError
        {
            public string Message => "url is required";
        }

        public class OwnerIdIsRequiredError : IError
        {
            public string Message => "ownerId is required";
        }

        public class ReceiversIdsAreRequiredError : IError
        {
            public string Message => "receiversIds are required";
        }

        public class NotEnoughReceiversError : IError
        {
            public string Message => "there are not enough receivers";
        }
    }
}

﻿namespace Fortu.Domain.Roles.Errors
{
    public static class RoleErrors
    {
        public class NameIsRequiredError : DefaultError { }
        public class NameIsTooLongError : DefaultError { }
        public class PermissionsCannotBeNullError : DefaultError { }
    }
}

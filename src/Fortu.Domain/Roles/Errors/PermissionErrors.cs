﻿namespace Fortu.Domain.Roles.Errors
{
    public static class PermissionErrors
    {
        public class NameIsRequired : DefaultError { }
        public class NameIsTooLong : DefaultError { }
    }
}

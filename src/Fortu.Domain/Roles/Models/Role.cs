﻿using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Roles.Errors;

namespace Fortu.Domain.Roles.Models
{
    public class Role : IAggregateRoot<string>
    {
        public string Id => Name;
        public string Name { get; }
        public IEnumerable<Permission> Permissions { get; }

        private Role(string name, IEnumerable<Permission> permissions)
        {
            Name = name;
            Permissions = permissions;
        }

        public static IResult<Role> Create(string name, IEnumerable<Permission> permissions)
        {
            var permissionsArray = permissions as Permission[] ?? permissions.ToArray();
            return Validate(name, permissionsArray)
                .Bind(() => new Role(name, permissionsArray).AsResult());
        }

        private static IResult<Unit> Validate(string name, IEnumerable<Permission> permissions)
        {
            IEnumerable<IError> Validate()
            {
                if (string.IsNullOrEmpty(name))
                    yield return new RoleErrors.NameIsRequiredError();
                else if (name.Length > 255)
                    yield return new RoleErrors.NameIsTooLongError();

                if(permissions is null)
                    yield return new RoleErrors.PermissionsCannotBeNullError();
            }

            var errors = Validate().ToArray();

            return errors.Any()
                ? ResultFactory.CreateFailure(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess();
        }
    }
}
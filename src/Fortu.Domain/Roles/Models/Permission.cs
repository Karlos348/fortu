﻿using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Roles.Errors;

namespace Fortu.Domain.Roles.Models
{
    public class Permission
    {
        public string Name { get; }

        private Permission(string name) 
            => Name = name;

        public static IResult<Permission> Create(string name)
            => Validate(name)
                .Bind(() => new Permission(name).AsResult());

        private static IResult<Unit> Validate(string name)
        {
            IEnumerable<IError> Validate()
            {
                if (string.IsNullOrEmpty(name))
                    yield return new PermissionErrors.NameIsRequired();
                else if (name.Length > 255)
                    yield return new PermissionErrors.NameIsTooLong();
            }

            var errors = Validate().ToArray();

            return errors.Any()
                ? ResultFactory.CreateFailure(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess();
        }
    }
}

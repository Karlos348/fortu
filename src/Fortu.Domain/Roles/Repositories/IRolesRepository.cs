﻿using Fortu.Domain.Roles.Models;

namespace Fortu.Domain.Roles.Repositories
{
    public interface IRolesRepository : IRepository<Role, string>
    {
        
    }
}
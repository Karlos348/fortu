﻿using System.Collections.Generic;
using System.Linq;
using Feree.ResultType.Results;

namespace Fortu.Domain
{
    public class ValidationErrors : IError
    {
        public string Message { get; }
        public IEnumerable<IError> Errors { get; }

        public ValidationErrors(IEnumerable<IError> errors)
        {
            Errors = errors;
            Message = $"found {Errors.Count()} validation errors";
        }
    }
}

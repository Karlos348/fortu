﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Results;

namespace Fortu.Domain
{
    public interface IRepository<TAggregateRoot, in TId>
        where TAggregateRoot : IAggregateRoot<TId>
    {
        Task<IResult<TAggregateRoot>> Get(TId id);
        Task<IResult<Unit>> Add(TAggregateRoot entity);
        Task<IResult<IEnumerable<TAggregateRoot>>> Find(Predicate<TAggregateRoot> predicate);
    }
}

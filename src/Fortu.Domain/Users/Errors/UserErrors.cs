﻿using Feree.ResultType.Results;

namespace Fortu.Domain.Users.Errors
{
    public static class UserErrors
    {
        public class IdIsRequiredError : IError
        {
            public string Message => "id is required";
        }

        public class NameIsRequiredError : IError
        {
            public string Message => "name is required";
        }
    }
}

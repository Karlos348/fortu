using System;
using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Users.Errors;

namespace Fortu.Domain.Users.Models
{
    public class User
    {
        public Guid Id { get; }
        public DateTimeOffset LastActiveAt { get; }
        public bool IsActive { get; }
        public string Name { get; }

        private User(Guid id, string name, bool isActive, DateTimeOffset lastActiveAt)
        {
            Id = id;
            Name = name;
            IsActive = isActive;
            LastActiveAt = lastActiveAt;
        }

        public static IResult<User> Reconstitute(Guid id, string name, bool isActive, DateTimeOffset lastActiveAt)
            => Validate(id, name)
                .Bind(_ => new User(id, name, isActive, lastActiveAt).AsResult());

        public static IResult<User> Create(string name, bool isActive)
            => Reconstitute(Guid.NewGuid(), name, isActive, DateTimeOffset.UtcNow);

        private static IResult<Unit> Validate(Guid id, string name)
        {
            IEnumerable<IError> GetErrors()
            {
                if (id == Guid.Empty)
                    yield return new UserErrors.IdIsRequiredError();

                if(string.IsNullOrEmpty(name))
                    yield return new UserErrors.NameIsRequiredError();
            }

            var errors = GetErrors().ToArray();

            return errors.Any()
                ? ResultFactory.CreateFailure(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess();
        }
    }
}
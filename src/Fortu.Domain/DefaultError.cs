﻿using Feree.ResultType.Results;

namespace Fortu.Domain
{
    public abstract class DefaultError : IError
    {
        public string Message => GetType().FullName;
    }
}

﻿using Feree.ResultType.Results;

namespace Fortu.Domain.Accounts.Errors
{
    public static class AccountErrors
    {
        public class NameIsRequiredError : IError
        {
            public string Message => "name is required";
        }

        public class NameIsTooShortError : IError
        {
            public string Message => "name must be at least 5 characters long";
        }

        public class NameIsTooLongError : IError
        {
            public string Message => "name must be shorter than 256 characters";
        }

        public class UserIdIsRequiredError : IError
        {
            public string Message => "userId is required";
        }
    }
}

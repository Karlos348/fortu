﻿using Feree.ResultType.Results;

namespace Fortu.Domain.Accounts.Errors
{
    public static class CredentialsErrors
    {
        public class PasswordIsRequiredError : IError
        {
            public string Message => "password is required";
        }

        public class PasswordIsTooShortError : IError
        {
            public string Message => "password must be at least 10 characters long";
        }

        public class PasswordIsTooLongError : IError
        {
            public string Message => "password must be shorter than 256 characters";
        }

        public class SaltIsRequiredError : IError
        {
            public string Message => "salt is required error";
        }

        public class IncorrectPassword : IError
        {
            public string Message => "password is incorrect";
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Accounts.Errors;

namespace Fortu.Domain.Accounts.Models
{
    public class Account : IAggregateRoot<string>
    {
        public string Id => Name;
        public string Name { get; }
        public Credentials Credentials { get; }
        public string RoleId { get; }
        public string UserId { get; }

        private Account(string name, Credentials credentials, string roleId, string userId)
        {
            Name = name;
            Credentials = credentials;
            RoleId = roleId;
            UserId = userId;
        }

        public static IResult<Account> Reconstitute(string name, Credentials credentials, string roleId, string userId)
            => Validate(name, userId)
                .Bind(_ => new Account(name, credentials, roleId, userId).AsResult());

        private static IResult<Account> Create(string name, string password, string roleId, string userId)
            => Credentials.Create(password)
                .Bind(credentials => Reconstitute(name, credentials, roleId, userId));

        public static IResult<Account> CreateUser(string name, string password, string userId)
            => Create(name, password, "user", userId);
        
        public static IResult<Account> CreateAdmin(string name, string password, string userId)
            => Create(name, password, "admin", userId);

        private static IResult<Unit> Validate(string name, string userId)
        {
            IEnumerable<IError> Validate()
            {
                if (name is null)
                    yield return new AccountErrors.NameIsRequiredError();
                else if (name.Length < 5)
                    yield return new AccountErrors.NameIsTooShortError();
                else if (name.Length > 255)
                    yield return new AccountErrors.NameIsTooLongError();

                if (string.IsNullOrEmpty(userId))
                    yield return new AccountErrors.UserIdIsRequiredError();
            }

            var errors = Validate().ToArray();

            return errors.Any()
                ? ResultFactory.CreateFailure(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess();
        }
    }
}

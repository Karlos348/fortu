﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Accounts.Errors;
using Fortu.Domain.Accounts.Services;

namespace Fortu.Domain.Accounts.Models
{
    public class Credentials
    {
        public string PasswordHashed { get; }
        public string Salt { get; }
        public int HashIterations { get; }
        public int HashLength { get; }

        private Credentials(string passwordHashed, string salt, int hashIterations, int hashLength)
        {
            PasswordHashed = passwordHashed;
            Salt = salt;
            HashIterations = hashIterations;
            HashLength = hashLength;
        }

        public static IResult<Credentials> Reconstitute(string passwordHashed, string salt, int hashIterations, int hashLength)
            => new Credentials(passwordHashed, salt, hashIterations, hashLength).AsResult();

        public static IResult<Credentials> Create(string password)
        {
            const int iterations = 10000;
            const int hashLength = 64;

            return GenerateSalt().AsResult()
                .Bind(salt => Validate(password, salt)
                    .Bind(() => PasswordHasher.Hash(password, salt, iterations, hashLength)
                        .Bind(passwordHashed => Reconstitute(passwordHashed, salt, iterations, hashLength))));
        }

        private static IResult<Unit> Validate(string password, string salt)
        {
            IEnumerable<IError> Validate()
            {
                if (password is null)
                    yield return new CredentialsErrors.PasswordIsRequiredError();
                else if (password.Length < 10)
                    yield return new CredentialsErrors.PasswordIsTooShortError();
                else if (password.Length > 255)
                    yield return new CredentialsErrors.PasswordIsTooLongError();

                if(string.IsNullOrEmpty(salt))
                    yield return new CredentialsErrors.SaltIsRequiredError();
            }

            var errors = Validate().ToArray();

            return errors.Any()
                ? ResultFactory.CreateFailure(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess();
        }

        private static string GenerateSalt()
            => Guid.NewGuid().ToString().Replace("-", string.Empty);

        public IResult<Unit> CheckPassword(string password)
            => PasswordHasher.Hash(password, Salt, HashIterations, HashLength)
                .Bind(passwordHashed =>
                    passwordHashed.Equals(PasswordHashed, StringComparison.InvariantCulture)
                    ? ResultFactory.CreateSuccess()
                    : ResultFactory.CreateFailure(new CredentialsErrors.IncorrectPassword()));
    }
}

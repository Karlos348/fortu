﻿using System.Threading.Tasks;
using Feree.ResultType.Results;
using Fortu.Domain.Accounts.Models;

namespace Fortu.Domain.Accounts.Repositories
{
    public interface IAccountsRepository : IRepository<Account, string>
    {
        Task<IResult<bool>> Exists(string name);
    }
}

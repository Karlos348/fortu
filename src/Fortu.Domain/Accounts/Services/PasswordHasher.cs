﻿using System;
using System.Text;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Fortu.Domain.Accounts.Services
{
    public static class PasswordHasher
    {
        public static IResult<string> Hash(string password, string salt, int iterations, int lengthInBytes)
        {
            if(salt.Length < 8)
                return ResultFactory.CreateFailure<string>("salt must be at least 8 characters long");

            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Encoding.UTF8.GetBytes(salt),
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: iterations,
                numBytesRequested: lengthInBytes)).AsResult();
        }
    }
}

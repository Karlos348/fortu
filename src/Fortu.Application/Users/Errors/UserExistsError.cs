﻿using Feree.ResultType.Errors;
using Feree.ResultType.Results;

namespace Fortu.Application.Users.Errors
{
    public class UserExistsError : IError
    {
        public string Message => "User is already exists.";
    }
}

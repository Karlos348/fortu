﻿using Feree.ResultType.Errors;
using Feree.ResultType.Results;

namespace Fortu.Application.Users.Errors
{
    public class UserNotLoggedError : IError
    {
        public string Message => "User is not logged.";
    }
}

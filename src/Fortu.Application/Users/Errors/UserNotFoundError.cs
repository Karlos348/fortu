﻿using Feree.ResultType.Results;

namespace Fortu.Application.Users.Errors
{
    public class UserNotFoundError : IError
    {
        public string Message => "User not found.";
    }
}

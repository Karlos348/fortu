﻿using Feree.ResultType.Errors;
using Feree.ResultType.Results;

namespace Fortu.Application.Users.Errors
{
    public class SaveUserError : IError
    {
        public string Message => "User not created.";
    }
}

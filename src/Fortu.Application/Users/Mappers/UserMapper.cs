﻿using Fortu.Application.Users.Dto;
using Fortu.Domain.Users.Models;

namespace Fortu.Application.Users.Mappers
{
    public static class UserMapper
    {
        public static UserDto Map(this User user)
            => new UserDto
            {
                Id = user.Id,
                Name = user.Name
            };
    }
}

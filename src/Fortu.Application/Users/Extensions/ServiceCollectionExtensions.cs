﻿using Fortu.Application.Users.Handlers;
using Fortu.Application.Users.Services;
using Fortu.Infrastructure.Users;
using Fortu.Mediator.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fortu.Application.Users.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddUsers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ILoggedUserProvider, LoggedUserProvider>();
        }
    }
}

﻿using System;
using System.Linq;
using Fortu.Infrastructure.Users;
using Microsoft.AspNetCore.Http;

namespace Fortu.Application.Users.Services
{
    public class LoggedUserProvider : ILoggedUserProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LoggedUserProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool IsLogged()
            => _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;

        public Guid? GetId()
            => IsLogged()
                ? Guid.Parse(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "id")?.Value)
                : (Guid?) null;
    }
}

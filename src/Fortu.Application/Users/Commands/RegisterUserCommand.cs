﻿using Fortu.Mediator.CQS.Commands;

namespace Fortu.Application.Users.Commands
{
    public class RegisterUserCommand : ICommand
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }

        public RegisterUserCommand(string username, string password, string name)
        {
            Username = username;
            Password = password;
            Name = name;
        }
    }
}

﻿using System;

namespace Fortu.Application.Users.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

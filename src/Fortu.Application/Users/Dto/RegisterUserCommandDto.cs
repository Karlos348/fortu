﻿namespace Fortu.Application.Users.Dto
{
    public class RegisterUserCommandDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}

﻿using Fortu.Application.Users.Dto;
using Fortu.Mediator.CQS.Queries;

namespace Fortu.Application.Users.Queries
{
    public class GetUserQuery : IQuery<UserDto>
    {
    }
}

﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Errors;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Application.Users.Commands;
using Fortu.Application.Users.Errors;
using Fortu.Domain.Users.Models;
using Fortu.Infrastructure.HttpResult;
using Fortu.Mediator.CQS.Commands;

namespace Fortu.Application.Users.Handlers
{
    /*public class RegisterUserCommandHandler : ICommandHandler<RegisterUserCommand>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IHasher _hasher;
        private readonly IAccountsRepository _accountsRepository;

        public RegisterUserCommandHandler(IUsersRepository usersRepository, IHasher hasher, IAccountsRepository accountsRepository)
        {
            _usersRepository = usersRepository;
            _hasher = hasher;
            _accountsRepository = accountsRepository;
        }

        public async Task<Mediator.CQS.Results.IResult> Handle(RegisterUserCommand command)
        {
            var result = await UserExists(command.Username)
                .BindAsync(() => new User(command.Name).AsResult()
                    .BindAsync(user => GenerateSalt().AsResult()
                        .BindAsync(salt => _hasher.Hash(command.PasswordHashed, salt).AsResult()
                            .BindAsync(hashedPassword =>
                                AttachCredentials(user, new Credentials(command.Username, hashedPassword, salt)).AsResult()
                                    .BindAsync(() => SaveUser(user))))));

            switch (result)
            {
                case Success _:
                    return HttpResultFactory.CreateSuccess(HttpStatusCode.Created);

                case Failure failure:
                    switch (failure.Error)
                    {
                        case UserExistsError _:
                            return HttpResultFactory.CreateFailure(HttpStatusCode.Conflict, failure.Error.Message);

                        case SaveUserError _:
                            return HttpResultFactory.CreateFailure(HttpStatusCode.InternalServerError, failure.Error.Message);

                        case IError _:
                            return HttpResultFactory.CreateFailure(HttpStatusCode.InternalServerError, failure.Error.Message);

                        default:
                            return HttpResultFactory.CreateFailure(HttpStatusCode.BadRequest, "User not created");
                    }

                default:
                    return HttpResultFactory.CreateFailure(HttpStatusCode.BadRequest, "User not created");
            }
        }

        private async Task<IResult<Unit>> UserExists(string username)
        {
            var result = await _accountsRepository.Exists(username);

            return result
                ? ResultFactory.CreateFailure(new UserExistsError())
                : ResultFactory.CreateSuccess();
        }

        private string GenerateSalt()
        {
            using (RandomNumberGenerator randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                byte[] saltData = new byte[64];
                randomNumberGenerator.GetNonZeroBytes(saltData);
                var result = Convert.ToBase64String(saltData);
                return result;
            }
        }

        private Unit AttachCredentials(User user, Credentials credentials)
        {
            //user.Credentials = credentials;
            return new Unit();
        }

        private async Task<IResult<Unit>> SaveUser(User user)
        {
            await _usersRepository.Add(user);
            var isSuccessful //= await _usersRepository.UnitOfWork.Commit() > 0;
                = true;
            return isSuccessful
                ? ResultFactory.CreateSuccess()
                : ResultFactory.CreateFailure<Unit>(new SaveUserError());
        }
    }*/
}

﻿using System;
using System.Net;
using System.Threading.Tasks;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Application.Users.Dto;
using Fortu.Application.Users.Errors;
using Fortu.Application.Users.Mappers;
using Fortu.Application.Users.Queries;
using Fortu.Domain.Users.Models;
using Fortu.Infrastructure.HttpResult;
using Fortu.Infrastructure.Users;
using Fortu.Mediator.CQS.Queries;

namespace Fortu.Application.Users.Handlers
{
    /*public class GetUserQueryHandler : IQueryHandler<GetUserQuery, UserDto>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly ILoggedUserProvider _loggedUserProvider;

        public GetUserQueryHandler(IUsersRepository usersRepository, ILoggedUserProvider loggedUserProvider)
        {
            _usersRepository = usersRepository;
            _loggedUserProvider = loggedUserProvider;
        }

        public async Task<Mediator.CQS.Results.IResult<UserDto>> Handle(GetUserQuery message)
        {
            var result = await GetLoggedUserId()
                .BindAsync(GetUser)
                .BindAsync(user => user.Map().AsResult());

            switch (result)
            {
                case Success<UserDto> success:
                    return HttpResultFactory.CreateSuccess(success.Payload, HttpStatusCode.OK);

                case Failure failure:
                    switch (failure.Error)
                    {
                        case UserNotFoundError _:
                            return HttpResultFactory.CreateFailure<UserDto>(HttpStatusCode.NotFound, failure.Error.Message);

                        case UserNotLoggedError _:
                            return HttpResultFactory.CreateFailure<UserDto>(HttpStatusCode.Forbidden, failure.Error.Message);

                        default:
                            throw new Exception();
                    }

                default:
                    throw new Exception();
            }
        }

        private IResult<Guid> GetLoggedUserId()
            => !_loggedUserProvider.IsLogged() 
                ? ResultFactory.CreateFailure<Guid>(new UserNotLoggedError()) 
                : ResultFactory.CreateSuccess(_loggedUserProvider.GetId().GetValueOrDefault());

        private async Task<IResult<User>> GetUser(Guid id)
        {
            var result = await _usersRepository.Get(id);

            return result is null 
                ? ResultFactory.CreateFailure<User>(new UserNotFoundError()) 
                : ResultFactory.CreateSuccess(result);
        }
    }*/
}

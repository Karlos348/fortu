﻿namespace Fortu.Application.Accounts.Settings
{
    public class JwtSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}

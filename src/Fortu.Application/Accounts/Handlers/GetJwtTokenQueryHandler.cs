﻿using System;
using System.Threading.Tasks;
using Feree.ResultType.Results;
using Fortu.Application.Accounts.Dto;
using Fortu.Application.Accounts.Queries;
using Fortu.Application.Accounts.Services;
using Fortu.Domain.Accounts.Repositories;
using Fortu.Domain.Roles.Repositories;
using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Handlers
{
    public class GetJwtTokenQueryHandler : IQueryHandler<GetJwtTokenQuery, JsonWebTokenDto>
    {
        private readonly IAccountsRepository _accountRepository;
        private readonly IRolesRepository _rolesRepository;
        private readonly IJwtService _jwtService;

        public GetJwtTokenQueryHandler(IAccountsRepository accountRepository, IRolesRepository rolesRepository,
            IJwtService jwtService)
        {
            _accountRepository = accountRepository;
            _rolesRepository = rolesRepository;
            _jwtService = jwtService;
        }

        public Task<IResult<JsonWebTokenDto>> Handle(GetJwtTokenQuery message)
            => _accountRepository.Get(message.Login)
                .BindAsync(account => account.Credentials.CheckPassword(message.Password)
                    .BindAsync(_ => _rolesRepository.Get(account.RoleId)
                        .BindAsync(role =>
                            _jwtService.GenerateToken(
                                ClaimsBuilder.NewClaims().SetId(account.Id).SetRole(role).SetPermissions(role),
                                TimeSpan.FromHours(12)))));
    }
}

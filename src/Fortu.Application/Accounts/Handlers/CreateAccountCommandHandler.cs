﻿using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Application.Accounts.Commands;
using Fortu.Application.Accounts.Errors;
using Fortu.Domain.Accounts.Models;
using Fortu.Domain.Accounts.Repositories;
using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Handlers
{
    public class CreateAccountCommandHandler : ICommandHandler<CreateAccountCommand>
    {
        private readonly IAccountsRepository _accountsRepository;

        public CreateAccountCommandHandler(IAccountsRepository accountsRepository)
            => _accountsRepository = accountsRepository;

        public Task<IResult<Unit>> Handle(CreateAccountCommand message)
            => _accountsRepository.Exists(message.Name)
                .BindAsync(isExists => isExists
                    ? ResultFactory.CreateFailure(new AccountAlreadyExistsError())
                    : ResultFactory.CreateSuccess())
                .BindAsync(_ => Account.CreateUser(message.Name, message.Password, "not_implemented")
                    .BindAsync(account => _accountsRepository.Add(account)));
    }
}

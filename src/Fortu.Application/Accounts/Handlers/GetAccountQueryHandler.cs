﻿using System.Threading.Tasks;
using Feree.ResultType.Results;
using Fortu.Application.Accounts.Queries;
using Fortu.Domain.Accounts.Models;
using Fortu.Domain.Accounts.Repositories;
using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Handlers
{
    public class GetAccountQueryHandler : IQueryHandler<GetAccountQuery, Account>
    {
        private readonly IAccountsRepository _accountsRepository;

        public GetAccountQueryHandler(IAccountsRepository accountsRepository) 
            => _accountsRepository = accountsRepository;

        public Task<IResult<Account>> Handle(GetAccountQuery message)
            => _accountsRepository.Get(message.Id);
    }
}

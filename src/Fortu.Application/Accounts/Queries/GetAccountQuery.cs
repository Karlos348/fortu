﻿using Fortu.Domain.Accounts.Models;
using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Queries
{
    public class GetAccountQuery : IQuery<Account>
    {
        public GetAccountQuery(string id) 
            => Id = id;

        public string Id { get; }
    }
}

﻿using Fortu.Application.Accounts.Dto;
using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Queries
{
    public class GetJwtTokenQuery : IQuery<JsonWebTokenDto>
    {
        public GetJwtTokenQuery(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public string Login { get; }
        public string Password { get; }
    }
}

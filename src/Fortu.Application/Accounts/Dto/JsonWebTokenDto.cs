﻿using System;

namespace Fortu.Application.Accounts.Dto
{
    public class JsonWebTokenDto
    {
        public string Token { get; }
        public DateTimeOffset ExpirationTime { get; }

        public JsonWebTokenDto(string token, DateTimeOffset expirationTime)
        {
            Token = token;
            ExpirationTime = expirationTime;
        }
    }
}
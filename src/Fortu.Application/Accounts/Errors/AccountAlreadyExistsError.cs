﻿using Feree.ResultType.Results;

namespace Fortu.Application.Accounts.Errors
{
    public class AccountAlreadyExistsError : IError
    {
        public string Message => "account already exists";
    }
}

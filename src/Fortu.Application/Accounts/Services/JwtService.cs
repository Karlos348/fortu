﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Application.Accounts.Dto;
using Fortu.Application.Accounts.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Fortu.Application.Accounts.Services
{
    public class JwtService : IJwtService
    {
        private readonly JwtSettings _jwtSettings;

        public JwtService(IOptionsSnapshot<JwtSettings> jwtSettingsOpt)
            => _jwtSettings = jwtSettingsOpt.Value;

        public IResult<JsonWebTokenDto> GenerateToken(IEnumerable<Claim> claims, TimeSpan lifetime)
            => DateTimeOffset.Now.Add(lifetime).AsResult()
                .Bind(expirationTime => GenerateToken(claims, expirationTime)
                    .Bind(token => new JsonWebTokenDto(token, expirationTime).AsResult()));

        private IResult<string> GenerateToken(IEnumerable<Claim> claims, DateTimeOffset expirationTime)
        {
            try
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
                var signinCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var data = new JwtSecurityToken(
                    issuer: _jwtSettings.Issuer,
                    expires: expirationTime.UtcDateTime,
                    claims: claims,
                    signingCredentials: signinCredentials
                );

                return new JwtSecurityTokenHandler().WriteToken(data).AsResult();
            }
            catch (Exception ex)
            {
                return ResultFactory.CreateFailure<string>(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Feree.ResultType.Results;
using Fortu.Application.Accounts.Dto;

namespace Fortu.Application.Accounts.Services
{
    public interface IJwtService
    {
        IResult<JsonWebTokenDto> GenerateToken(IEnumerable<Claim> claims, TimeSpan lifetime);
    }
}

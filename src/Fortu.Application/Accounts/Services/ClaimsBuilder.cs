﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Fortu.Domain.Roles.Models;

namespace Fortu.Application.Accounts.Services
{
    public static class ClaimsBuilder
    {
        public static IEnumerable<Claim> NewClaims()
            => Enumerable.Empty<Claim>();

        public static IEnumerable<Claim> SetId(this IEnumerable<Claim> claims, string id)
            => claims.Add(new Claim("id", id));

        public static IEnumerable<Claim> SetRole(this IEnumerable<Claim> claims, Role role) 
            => claims.Add(new Claim("role", role.Name));
        
        public static IEnumerable<Claim> SetPermissions(this IEnumerable<Claim> claims, Role role)
            => claims.Add(new Claim("permissions", string.Join(',', role.Permissions.Select(x => x.Name))));

        private static IEnumerable<Claim> Add(this IEnumerable<Claim> claims, Claim claim)
        {
            if (claims != null)
                foreach (var item in claims)
                    yield return item;

            yield return claim;
        }
    }
}

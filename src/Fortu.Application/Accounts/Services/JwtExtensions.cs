﻿using System;
using System.Text;
using Fortu.Application.Accounts.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Fortu.Application.Accounts.Services
{
    public static class JwtExtensions
    {
        public static void AddJwtService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IJwtService, JwtService>();
            services.Configure<JwtSettings>(configuration.GetSection("jwt"));
        }

        public static void AddJwtAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            JwtSettings settings = new JwtSettings();
            configuration.GetSection("jwt").Bind(settings);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = settings.Issuer,
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = false,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Key)),
                        ValidateActor = false,
                        ClockSkew = TimeSpan.Zero
                    };
                });
            services.AddAuthorization();
        }
    }
}

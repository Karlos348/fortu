﻿using Fortu.Infrastructure.CQRS;

namespace Fortu.Application.Accounts.Commands
{
    public class CreateAccountCommand : ICommand
    {
        public CreateAccountCommand(string name, string password)
        {
            Name = name;
            Password = password;
        }

        public string Name { get; }
        public string Password { get; }
    }
}

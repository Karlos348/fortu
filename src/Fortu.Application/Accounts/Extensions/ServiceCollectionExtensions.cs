﻿using System;
using System.Text;
using Fortu.Application.Accounts.Handlers;
using Fortu.Application.Accounts.Services;
using Fortu.Application.Accounts.Settings;
using Fortu.Infrastructure.Authorization;
using Fortu.Mediator.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Fortu.Application.Accounts.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            services.RegisterHandler<GetJwtTokenQueryHandler>();
            services.RegisterHandler<CreateAccountCommandHandler>();
            services.RegisterHandler<GetAccountQueryHandler>();
            services.AddJwtService(configuration);
            services.AddJwtAuthorization(configuration);
            services.AddPermissionsRequirement();
        }

        private static void AddJwtService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IJwtService, JwtService>();
            services.Configure<JwtSettings>(configuration.GetSection("jwt"));
        }

        private static void AddJwtAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            JwtSettings settings = new JwtSettings();
            configuration.GetSection("jwt").Bind(settings);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = settings.Issuer,
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = false,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Key)),
                        ValidateActor = false,
                        ClockSkew = TimeSpan.Zero
                    };
                });
        }

        private static IServiceCollection AddPermissionsRequirement(this IServiceCollection services) =>
            services.AddAuthorization(x =>
            {
                x.AddPolicy("Permissions", p =>
                {
                    p.AddRequirements(new PermissionsAuthorizationRequirement());
                    p.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                });
                x.AddPolicy("Permissions2", p =>
                {
                    p.Requirements.Add(new PermissionsAuthorizationRequirement());
                    p.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                });
                x.DefaultPolicy = x.GetPolicy("Permissions");
            });
    }
}

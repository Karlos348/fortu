﻿namespace Fortu.Application.Settings
{
    public class ApplicationInfo
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}

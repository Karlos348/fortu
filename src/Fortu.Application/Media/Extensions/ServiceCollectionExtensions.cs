﻿using System.Reflection;
using FileSignatures;
using Fortu.Application.Media.Handlers;
using Fortu.Application.Media.Services;
using Fortu.Application.Media.Settings;
using Fortu.Infrastructure.FileAnalyzer;
using Fortu.Infrastructure.FilesSignatures;
using Fortu.Infrastructure.Persistence.MongoDB.Settings;
using Fortu.Mediator.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fortu.Application.Media.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMedia(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<NoSqlDatabaseSettings>(configuration.GetSection("noSqlDatabase"));
            services.Configure<MediaStorageSettings>(configuration.GetSection("blobStorage"));
            services.AddScoped<IMediaStorage, MediaStorage>();
            //services.AddScoped<IFileAnalyzer, FileAnalyzer>();
            var assembly = typeof(Video).GetTypeInfo().Assembly;
            var allFormats = FileFormatLocator.GetFormats(assembly, true);
            services.AddScoped<IFileFormatInspector>(x => new FileFormatInspector(allFormats));
        }
    }
}

﻿using Fortu.Mediator.CQS.Commands;
using Microsoft.AspNetCore.Http;

namespace Fortu.Application.Media.Commands
{
    public class AddMediaCommand : ICommandExtended<string>
    {
        public IFormFile File { get; set; }
        public int Receivers { get; set; }
    }
}

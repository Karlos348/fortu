using Fortu.Application.Media.Settings;
using Fortu.Infrastructure.Persistence.Storage.Azure;
using Microsoft.Extensions.Options;

namespace Fortu.Application.Media.Services
{
    public class MediaStorage : AzureBlobStorage<MediaStorageSettings>, IMediaStorage
    {
        public MediaStorage(IOptionsSnapshot<MediaStorageSettings> storageSettings) 
            : base(storageSettings)
        {
        }
    }
}
﻿using Fortu.Application.Media.Settings;
using Fortu.Infrastructure.Persistence.Storage.Azure;

namespace Fortu.Application.Media.Services
{
    public interface IMediaStorage : IAzureBlobStorage<MediaStorageSettings>
    {
    }
}

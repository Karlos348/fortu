﻿using Fortu.Infrastructure.Persistence.Storage.Azure;

namespace Fortu.Application.Media.Settings
{
    public class MediaStorageSettings : IAzureBlobStorageSettings
    {
        public string Url { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Application.Media.Commands;
using Fortu.Application.Media.Services;
using Fortu.Domain.Media.Models;
using Fortu.Domain.Media.Repositories;
using Fortu.Infrastructure.FileAnalyzer;
using Fortu.Infrastructure.HttpResult;
using Fortu.Infrastructure.Users;
using Fortu.Mediator.CQS.Commands;
using Microsoft.Extensions.Logging;

namespace Fortu.Application.Media.Handlers
{
    /*public class AddMediaCommandHandler : ICommandExtendedHandler<AddMediaCommand, string>
    {
        private readonly IMediaRepository _mediaRepository;
        private readonly ILoggedUserProvider _loggedUserProvider;
        private readonly IMediaStorage _mediaStorage;
        private readonly IFileAnalyzer _fileAnalyzer;
        private readonly ILogger<AddMediaCommandHandler> _logger;
        private readonly IUsersRepository _usersRepository;

        public AddMediaCommandHandler(IMediaRepository mediaRepository, ILoggedUserProvider loggedUserProvider, 
            IMediaStorage mediaStorage, IFileAnalyzer fileAnalyzer, ILogger<AddMediaCommandHandler> logger,
            IUsersRepository usersRepository)
        {
            _mediaRepository = mediaRepository;
            _loggedUserProvider = loggedUserProvider;
            _mediaStorage = mediaStorage;
            _fileAnalyzer = fileAnalyzer;
            _logger = logger;
            _usersRepository = usersRepository;
        }

        public Task<Mediator.CQS.Results.IResult<string>> Handle(AddMediaCommand command)
            => _fileAnalyzer.GetMimeType(command.File.OpenReadStream()).AsResult()
                .BindAsync(mimeType => 
                        _mediaStorage.Upload(
                                "media", 
                                Guid.NewGuid().ToString().Replace("-", string.Empty), 
                                mimeType, 
                                command.File.OpenReadStream())
                    .BindAsync(uri => DefineMediaType(mimeType).AsResult()
                        .BindAsync(mediaType => GetRandomReceivers(command.Receivers)
                            .BindAsync(receivers => AddMediaDocument(mediaType, uri.ToString(), receivers)))
                        .BindAsync(uri.ToString().AsResult)))
                .MapAsync(
                    uri => HttpResultFactory.CreateSuccess(uri, HttpStatusCode.OK),
                    failure =>
                    {
                        switch (failure)
                        {
                            case HttpError error:
                                return HttpResultFactory.CreateFailure<string>(error.HttpStatusCode, error.Message);

                            default:
                                throw new InvalidOperationException();
                        }
                    });

        private MediaType DefineMediaType(string mimeType)
        {
            if (mimeType.StartsWith("image"))
                return MediaType.Image;

            if (mimeType.StartsWith("video"))
                return MediaType.Video;

            if (mimeType.StartsWith("audio"))
                return MediaType.Audio;

            return MediaType.Unknown;
        }

        private Task<IResult<Unit>> AddMediaDocument(MediaType mediaType, string mediaUri, IEnumerable<Guid> receiversIds)
        {
            //var media = new Domain.Media.Media(mediaType, _loggedUserProvider.GetId().GetValueOrDefault(), mediaUri);
            //media.Receivers.AddRange(receiversIds);
            //await _mediaRepository.Add(media);
            return ResultFactory.CreateSuccessAsync();
        }

        private async Task<IResult<IEnumerable<Guid>>> GetRandomReceivers(int limit)
        {
            var result = await _usersRepository.GetRandomUsersIds(limit, TimeSpan.FromDays(100), _loggedUserProvider.GetId().GetValueOrDefault());
            if (result is null || !result.Any())
                return ResultFactory.CreateFailure<List<Guid>>(new HttpError(HttpStatusCode.NotFound,
                    "No receivers found."));

            return ResultFactory.CreateSuccess(result);
        }
    }*/
}

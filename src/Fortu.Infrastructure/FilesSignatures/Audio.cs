﻿using FileSignatures;

namespace Fortu.Infrastructure.FilesSignatures
{
    public abstract class Audio : FileFormat
    {
        protected Audio(byte[] signature, string mediaType, string extension) : base(signature, mediaType, extension)
        {
        }
    }
}

﻿using FileSignatures;

namespace Fortu.Infrastructure.FilesSignatures
{
    public abstract class Video : FileFormat
    {
        protected Video(byte[] signature, string mediaType, string extension) : base(signature, mediaType, extension)
        {
        }
    }
}

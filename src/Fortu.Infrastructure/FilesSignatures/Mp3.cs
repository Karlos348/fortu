﻿namespace Fortu.Infrastructure.FilesSignatures
{
    public class Mp3 : Audio
    {
        private static byte[] signature => new byte[] {0x49, 0x44, 0x33};
        private const string mediaType = "audio/mpeg";
        private const string extension = "mp3";

        public Mp3() : base(signature, mediaType, extension)
        {

        }
    }
}

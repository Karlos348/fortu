﻿namespace Fortu.Infrastructure.FilesSignatures
{
    public class WebM : Video
    {        
        private static byte[] signature => new byte[] { 0x1A, 0x45, 0xDF, 0xA3 };
        private const string mediaType = "video/webm";
        private const string extension = "webm";

        public WebM() : base(signature, mediaType, extension)
        {
        }
    }
}

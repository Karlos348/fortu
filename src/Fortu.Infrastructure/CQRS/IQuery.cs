﻿using Feree.ResultType.Results;
using Fortu.Mediator;

namespace Fortu.Infrastructure.CQRS
{
    public interface IQuery<out TResult> : IMessage<IResult<TResult>>
    {
    }
}

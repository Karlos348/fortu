﻿using Feree.ResultType;
using Feree.ResultType.Results;
using Fortu.Mediator;

namespace Fortu.Infrastructure.CQRS
{
    public interface ICommandHandler<in TCommand> : IMessageHandler<TCommand, IResult<Unit>>
        where TCommand : ICommand
    {
    }
}

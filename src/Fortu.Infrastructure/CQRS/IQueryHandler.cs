﻿using Feree.ResultType.Results;
using Fortu.Mediator;

namespace Fortu.Infrastructure.CQRS
{
    public interface IQueryHandler<in TQuery, TResult> : IMessageHandler<TQuery, IResult<TResult>>
        where TQuery : IQuery<TResult>
    {
    }
}

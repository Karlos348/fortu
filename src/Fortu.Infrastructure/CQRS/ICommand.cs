﻿using Feree.ResultType;
using Feree.ResultType.Results;
using Fortu.Mediator;

namespace Fortu.Infrastructure.CQRS
{
    public interface ICommand : IMessage<IResult<Unit>>
    {
    }
}

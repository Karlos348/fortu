﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace Fortu.Infrastructure.Authorization
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RequiredPermissions2Attribute : AuthorizeAttribute
    {
        public IEnumerable<string> Permissions { get; }

        public RequiredPermissions2Attribute(params string[] permissions) : base("Permissions2")
            => Permissions = permissions;
    }
}
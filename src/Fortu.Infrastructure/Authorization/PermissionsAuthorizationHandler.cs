﻿using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;

namespace Fortu.Infrastructure.Authorization
{
    public class PermissionsAuthorizationHandler : AuthorizationHandler<PermissionsAuthorizationRequirement>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionsAuthorizationRequirement handler)
        {
            var user = context.User;
            if (!user.Identity.IsAuthenticated || !user.HasClaim(x => x.Type == "permissions"))
            {
                context.Fail();
                return Task.CompletedTask;
            }
            
            var requiredPermissions = RequiredPermissions(context);
            if (!requiredPermissions.Any())
            {
                context.Succeed(handler);
                return Task.CompletedTask;
            }
            
            const string superPermission = "rl9";
            var userPermissions = user.Claims.First(x => x.Type == "permissions").Value.Split(',');
            if (userPermissions.Contains(superPermission) || requiredPermissions.All(x => userPermissions.Contains(x)))
                context.Succeed(handler);
            else
                context.Fail();
            
            return Task.CompletedTask;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string[] RequiredPermissions(AuthorizationHandlerContext context) =>
            context.Resource is RouteEndpoint routeEndpoint
                ? routeEndpoint.Metadata.OfType<RequiredPermissionsAttribute>()
                    .SelectMany(x => x.Permissions)
                    .ToArray()
                : new string[0];
    }
}
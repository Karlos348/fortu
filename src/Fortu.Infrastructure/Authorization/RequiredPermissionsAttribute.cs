﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace Fortu.Infrastructure.Authorization
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RequiredPermissionsAttribute : AuthorizeAttribute
    {
        public IEnumerable<string> Permissions { get; }

        public RequiredPermissionsAttribute(params string[] permissions) : base("Permissions")
            => Permissions = permissions;
    }
}
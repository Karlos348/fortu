﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Accounts.Models;
using Fortu.Domain.Accounts.Repositories;
using Fortu.Infrastructure.MockRepositories.Error;
using Fortu.Infrastructure.MockRepositories.Extensions;

namespace Fortu.Infrastructure.MockRepositories
{
    internal class AccountsRepositoryMock : IAccountsRepository
    {
        private static readonly HashSet<Account> Repository = new HashSet<Account>();

        public AccountsRepositoryMock()
            => FillRepository();

        public async Task<IResult<Account>> Get(string id)
        {
            await Task.CompletedTask;
            var result = Repository.FirstOrDefault(x => x.Name == id);
            return result is null 
                ? ResultFactory.CreateFailure<Account>(new AccountNotFoundError(id))
                : result.AsResult();
        }

        public Task<IResult<Unit>> Add(Account account)
        {
            Repository.Add(account);
            return ResultFactory.CreateSuccessAsync();
        }

        public async Task<IResult<IEnumerable<Account>>> Find(Predicate<Account> predicate)
        {
            await Task.CompletedTask;
            return Repository.Where(x => predicate(x)).AsResult();
        }

        public async Task<IResult<bool>> Exists(string name)
        {
            await Task.CompletedTask;
            return Repository.Any(x => x.Name == name).AsResult();
        }

        private static void FillRepository()
        {
            Repository.Add(Account.CreateUser("name1", "password100", "not_implemented").GetPayload());
            Repository.Add(Account.CreateUser("name2", "password200", "not_implemented").GetPayload());
            Repository.Add(Account.CreateAdmin("name3", "password300", "not_implemented").GetPayload());
        }
    }
}

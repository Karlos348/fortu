﻿using Fortu.Domain.Accounts.Repositories;
using Fortu.Domain.Roles.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Fortu.Infrastructure.MockRepositories.Extensions
{
    public static class ServicesCollectionExtensions
    {
        public static IServiceCollection AddMockRepository(this IServiceCollection services)
        {
            services.AddScoped<IRolesRepository, RolesRepositoryMock>();
            return services.AddScoped<IAccountsRepository, AccountsRepositoryMock>();
        }
    }
}

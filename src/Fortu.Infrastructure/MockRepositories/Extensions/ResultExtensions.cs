﻿using System;
using System.Threading.Tasks;
using Feree.ResultType.Results;

namespace Fortu.Infrastructure.MockRepositories.Extensions
{
    internal static class ResultExtensions
    {
        public static T GetPayload<T>(this IResult<T> value)
            => value is Success<T> success
                ? success.Payload
                : throw new InvalidCastException();
        
        public static async Task<T> GetPayloadAsync<T>(this Task<IResult<T>> valueTask)
            => (await valueTask) is Success<T> success
                ? success.Payload
                : throw new InvalidCastException();
    }
}
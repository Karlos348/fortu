﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Converters;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Fortu.Domain.Roles.Models;
using Fortu.Domain.Roles.Repositories;
using Fortu.Infrastructure.MockRepositories.Error;
using Fortu.Infrastructure.MockRepositories.Extensions;

namespace Fortu.Infrastructure.MockRepositories
{
    public class RolesRepositoryMock : IRolesRepository
    {
        private static readonly HashSet<Role> Repository = new HashSet<Role>();

        public RolesRepositoryMock() => FillRepository();

        public async Task<IResult<Role>> Get(string id)
        {
            await Task.CompletedTask;
            var result = Repository.FirstOrDefault(x => x.Name == id);
            return result is null 
                ? ResultFactory.CreateFailure<Role>(new RoleNotFoundError(id))
                : result.AsResult();
        }

        public Task<IResult<Unit>> Add(Role entity)
        {
            Repository.Add(entity);
            return ResultFactory.CreateSuccessAsync();
        }

        public async Task<IResult<IEnumerable<Role>>> Find(Predicate<Role> predicate)
        {
            await Task.CompletedTask;
            return Repository.Where(x => predicate(x)).AsResult();
        }
        
        private static void FillRepository()
        {
            Repository.Add(Role.Create("admin", new []{Permission.Create("rl9").GetPayload()}).GetPayload());
            Repository.Add(Role.Create("user", new []{Permission.Create("permission").GetPayload()}).GetPayload());
        }
    }
}
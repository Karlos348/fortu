﻿using Feree.ResultType.Results;

namespace Fortu.Infrastructure.MockRepositories.Error
{
    public class RoleNotFoundError : IError
    {
        public string Message { get; }

        public RoleNotFoundError(string name)
            => Message = $"role {name} not found";
    }
}
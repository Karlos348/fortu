﻿using Feree.ResultType.Results;

namespace Fortu.Infrastructure.MockRepositories.Error
{
    public class AccountNotFoundError : IError
    {
        public string Message { get; }

        public AccountNotFoundError(string name)
            => Message = $"account with name {name} not found";
    }
}

﻿using System.IO;

namespace Fortu.Infrastructure.FileAnalyzer
{
    public interface IFileAnalyzer
    {
        string GetMimeType(Stream fileStream);
    }
}

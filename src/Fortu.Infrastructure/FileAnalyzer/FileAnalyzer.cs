﻿using System.IO;
using FileSignatures;

namespace Fortu.Infrastructure.FileAnalyzer
{
    class FileAnalyzer : IFileAnalyzer
    {
        private readonly IFileFormatInspector _fileFormatInspector;

        public FileAnalyzer(IFileFormatInspector fileFormatInspector)
            => _fileFormatInspector = fileFormatInspector;

        public string GetMimeType(Stream fileStream)
            => _fileFormatInspector.DetermineFileFormat(fileStream)?.MediaType;
    }
}

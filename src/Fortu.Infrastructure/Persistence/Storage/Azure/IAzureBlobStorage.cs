namespace Fortu.Infrastructure.Persistence.Storage.Azure
{
    public interface IAzureBlobStorage<TSettings> : IStorage
        where TSettings : class, IAzureBlobStorageSettings, new() 
    {
    }
}
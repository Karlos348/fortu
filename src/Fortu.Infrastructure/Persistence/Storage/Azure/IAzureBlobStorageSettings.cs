namespace Fortu.Infrastructure.Persistence.Storage.Azure
{
    public interface IAzureBlobStorageSettings
    {
        string Url { get; set; }
        string AccountName { get; set; }
        string AccountKey { get; set; }
    }
}
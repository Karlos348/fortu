﻿using System;
using System.IO;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Factories;
using Feree.ResultType.Results;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Options;

namespace Fortu.Infrastructure.Persistence.Storage.Azure
{
    public class AzureBlobStorage<TSettings> : IAzureBlobStorage<TSettings>
        where TSettings : class, IAzureBlobStorageSettings, new()
    {
        private readonly TSettings _storageSettings;
        private readonly CloudBlobClient _cloudBlobClient;

        public AzureBlobStorage(IOptionsSnapshot<TSettings> storageSettings)
        {
            _storageSettings = storageSettings.Value;
            _cloudBlobClient = new CloudBlobClient(
                new StorageUri(new Uri(_storageSettings.Url)),
                new StorageCredentials(_storageSettings.AccountName, _storageSettings.AccountKey));
        }

        public async Task<IResult<Uri>> Upload(string container, string filename, string mediaType, Stream stream)
        {
            var containerReference = _cloudBlobClient.GetContainerReference(container);
            var blockBlobReference = containerReference.GetBlockBlobReference(filename);
            blockBlobReference.Properties.ContentType = mediaType;

            await blockBlobReference.UploadFromStreamAsync(stream);

            return ResultFactory.CreateSuccess(new Uri($"{_storageSettings.Url.TrimEnd('/')}/{container}/{filename}"));
        }

        public async Task<IResult<Unit>> Delete(string container, string filename)
        {
            var containerReference = _cloudBlobClient.GetContainerReference(container);
            var blockBlobReference = containerReference.GetBlockBlobReference(filename);
            await blockBlobReference.DeleteAsync();

            return ResultFactory.CreateSuccess();
        }
    }
}

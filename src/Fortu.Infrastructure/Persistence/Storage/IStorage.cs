﻿using System;
using System.IO;
using System.Threading.Tasks;
using Feree.ResultType;
using Feree.ResultType.Results;

namespace Fortu.Infrastructure.Persistence.Storage
{
    public interface IStorage
    {
        Task<IResult<Uri>> Upload(string container, string filename, string mediaType, Stream stream);
        Task<IResult<Unit>> Delete(string container, string filename);
    }
}

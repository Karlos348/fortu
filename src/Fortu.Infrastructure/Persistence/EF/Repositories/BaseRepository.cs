﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Fortu.Infrastructure.Users;
using Microsoft.EntityFrameworkCore;

namespace Fortu.Infrastructure.Persistence.EF.Repositories
{
    public abstract class BaseRepository<TEntity, TId>
        where TEntity : class, new()
    {
        protected readonly DbContext Context;

        protected BaseRepository(DbContext context, ILoggedUserProvider loggedUserProvider)
            => Context = context;

        protected virtual async Task<TId> Add(TEntity entity)
        {
            await Context.Set<TEntity>().AddAsync(entity);
            await Context.SaveChangesAsync(CancellationToken.None);
            return default;
        }

        protected virtual Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
            => Context.Set<TEntity>().Where(predicate).ToListAsync();

        protected virtual Task<TEntity> Get(TId id)
            => Context.Set<TEntity>().FindAsync(id).AsTask();
    }
}

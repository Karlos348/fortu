﻿namespace Fortu.Infrastructure.Persistence.MongoDB.Settings
{
    public class NoSqlDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}

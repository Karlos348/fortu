﻿using System.Collections.Generic;
using System.Net;
using Fortu.Mediator.CQS.Results;

namespace Fortu.Infrastructure.HttpResult
{
    public static class HttpResultFactory
    {
        public static IResult<TPayload> CreateSuccess<TPayload>(TPayload payload, HttpStatusCode httpStatusCode)
            => ResultFactory.Create(payload, new Metadata(httpStatusCode, true));

        public static IResult CreateSuccess(HttpStatusCode httpStatusCode)
            => ResultFactory.Create(new Metadata(httpStatusCode, true));

        public static IResult<TPayload> CreateFailure<TPayload>(HttpStatusCode httpStatusCode, string error)
            => ResultFactory.Create<TPayload>(new FailureMetadata(httpStatusCode, error));

        public static IResult<TPayload> CreateFailure<TPayload>(HttpStatusCode httpStatusCode, IEnumerable<string> errors)
            => ResultFactory.Create<TPayload>(new FailureMetadata(httpStatusCode, errors));

        public static IResult CreateFailure(HttpStatusCode httpStatusCode, string error)
            => ResultFactory.Create(new FailureMetadata(httpStatusCode, error));

        public static IResult CreateFailure(HttpStatusCode httpStatusCode, IEnumerable<string> errors)
            => ResultFactory.Create(new FailureMetadata(httpStatusCode, errors));
    }
}

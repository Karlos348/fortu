﻿using Fortu.Mediator.CQS.Results;
using Microsoft.AspNetCore.Mvc;

namespace Fortu.Infrastructure.HttpResult
{
    internal static class ActionResultMapper
    {
        public static IActionResult Map(IResult httpResult)
            => new JsonResult(httpResult)
            {
                StatusCode = (int) httpResult.Metadata.Extract<Metadata>().HttpStatusCode,
                ContentType = "application/json"
            };

        public static IActionResult Map<TPayload>(IResult<TPayload> httpResult)
            => new JsonResult(httpResult)
            {
                StatusCode = (int)httpResult.Metadata.Extract<Metadata>().HttpStatusCode,
                ContentType = "application/json"
            };
    }
}

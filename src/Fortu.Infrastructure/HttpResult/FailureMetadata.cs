﻿using System.Collections.Generic;
using System.Net;

namespace Fortu.Infrastructure.HttpResult
{
    internal class FailureMetadata : Metadata
    {
        public IEnumerable<string> Errors { get; }

        public FailureMetadata(HttpStatusCode httpStatusCode, string error)
            : base(httpStatusCode, false)
        {
            IEnumerable<string> SetError()
            {
                yield return error;
            }

            Errors = SetError();
        }

        public FailureMetadata(HttpStatusCode httpStatusCode, IEnumerable<string> errors)
            : base(httpStatusCode, false)
            => Errors = errors;
    }
}

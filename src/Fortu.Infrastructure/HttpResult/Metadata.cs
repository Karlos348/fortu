﻿using System.Net;
using Fortu.Mediator.CQS.Results;

namespace Fortu.Infrastructure.HttpResult
{
    internal class Metadata : IResultMetadata
    {
        public bool IsSuccessful { get; }
        public HttpStatusCode HttpStatusCode { get; }

        public Metadata(HttpStatusCode httpStatusCode, bool isSuccessful)
        {
            HttpStatusCode = httpStatusCode;
            IsSuccessful = isSuccessful;
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;

namespace Fortu.Infrastructure.HttpResult
{
    public static class ServiceCollectionExtensions
    {
        public static void AddHttpResult(this IServiceCollection services)
        {
            services.AddScoped<Mediator.CQS.IEventDispatcher, Mediator.CQS.EventDispatcher>();
        } 
    }
}

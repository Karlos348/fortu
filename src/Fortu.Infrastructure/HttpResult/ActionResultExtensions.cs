﻿using System.Threading.Tasks;
using Fortu.Mediator.CQS.Results;
using Microsoft.AspNetCore.Mvc;

namespace Fortu.Infrastructure.HttpResult
{
    public static class ActionResultExtensions
    {
        public static IActionResult AsActionResult(this IResult httpResult)
            => JsonResult(httpResult);

        public static IActionResult AsActionResult<TPayload>(this IResult<TPayload> httpResult)
            => JsonResult(httpResult);

        public static async Task<IActionResult> AsActionResultAsync(this Task<IResult> httpResultTask)
        {
            var result = await httpResultTask;
            return JsonResult(result);
        }

        public static async Task<IActionResult> AsActionResultAsync<TPayload>(this Task<IResult<TPayload>> httpResultTask)
        {
            var result = await httpResultTask;
            return JsonResult(result);
        }

        private static JsonResult JsonResult(IResult result)
            => new JsonResult(result)
            {
                StatusCode = (int) result.Metadata.Extract<Metadata>().HttpStatusCode,
                ContentType = "application/json"
            };
    }
}

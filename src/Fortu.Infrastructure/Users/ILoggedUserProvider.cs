﻿using System;

namespace Fortu.Infrastructure.Users
{
    public interface ILoggedUserProvider
    {
        bool IsLogged();
        Guid? GetId();
    }
}

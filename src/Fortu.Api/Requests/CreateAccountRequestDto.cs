﻿namespace Fortu.Api.Requests
{
    public class CreateAccountRequest
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Feree.ResultType.Results;
using Fortu.Api.Errors;
using Fortu.Application.Accounts.Commands;
using Fortu.Application.Accounts.Queries;
using Microsoft.AspNetCore.Http;
using Feree.ResultType.Factories;

namespace Fortu.Api.Factories
{
    public class AccountMessagesFactory
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountMessagesFactory(IHttpContextAccessor httpContextAccessor)
            => _httpContextAccessor = httpContextAccessor;

        public IResult<GetJwtTokenQuery> CreateGetJwtTokenQuery()
            => GetAuthorizationHeader()
                .Bind(authorizationHeader =>
                {
                    if (!authorizationHeader.StartsWith("Basic"))
                        return ResultFactory.CreateFailure<GetJwtTokenQuery>(new InvalidAuthorizationHeaderError());

                    var credentialsDecoded =
                        Convert.FromBase64String(authorizationHeader.Substring("Basic".Length).Trim());

                    if (credentialsDecoded.Count(x => x == ':') != 1)
                        return ResultFactory.CreateFailure<GetJwtTokenQuery>(new AuthorizationHeaderParsingError());

                    var credentials = Encoding.UTF8.GetString(credentialsDecoded).Split(":");

                    return ResultFactory.CreateSuccess(new GetJwtTokenQuery(credentials[0], credentials[1]));
                });

        public IResult<CreateAccountCommand> CreateCreateAccountCommand(string name, string password)
        {
            IEnumerable<IResult<CreateAccountCommand>> GetValidationErrors()
            {
                if (string.IsNullOrEmpty(name))
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new NameIsRequiredError());
                else if (name.Length > 256)
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new TooLongNameError());
                else if (name.Length < 5)
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new TooShortName());

                if (string.IsNullOrEmpty(password))
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new PasswordIsRequired());
                else if (password.Length > 256)
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new TooLongPassword());
                else if (password.Length < 10)
                    yield return ResultFactory.CreateFailure<CreateAccountCommand>(new TooShortPassword());
            }

            if (IsAuthenticated())
                return ResultFactory.CreateFailure<CreateAccountCommand>(new AccountAlreadyAuthenticatedError());

            var errors = GetValidationErrors().Select(x => ((Failure<CreateAccountCommand>) x).Error);

            return errors.Any()
                ? ResultFactory.CreateFailure<CreateAccountCommand>(new ValidationErrors(errors))
                : ResultFactory.CreateSuccess(new CreateAccountCommand(name, password));
        }

        public IResult<GetAccountQuery> CreateGetAccountQuery()
        {
            if (!IsAuthenticated())
                return ResultFactory.CreateFailure<GetAccountQuery>("xD");

            return ResultFactory.CreateSuccess(new GetAccountQuery(GetId()));
        }

        private IResult<string> GetAuthorizationHeader()
        {
            if (IsAuthenticated())
                return ResultFactory.CreateFailure<string>(new AccountAlreadyAuthenticatedError());

            var authorizationHeader = _httpContextAccessor.HttpContext?.Request?.Headers["Authorization"];
            return string.IsNullOrEmpty(authorizationHeader) 
                ? ResultFactory.CreateFailure<string>(new InvalidAuthorizationHeaderError()) 
                : ResultFactory.CreateSuccess(authorizationHeader.ToString());
        }

        private bool IsAuthenticated()
            => _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;

        private string GetId()
            => _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
    }
}

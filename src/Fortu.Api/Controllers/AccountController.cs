﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Feree.ResultType.Results;
using Fortu.Api.Errors;
using Fortu.Api.Factories;
using Fortu.Api.Requests;
using Fortu.Application.Accounts.Dto;
using Fortu.Application.Accounts.Errors;
using Fortu.Domain.Accounts.Errors;
using Fortu.Infrastructure.Authorization;
using Fortu.Infrastructure.HttpResult;
using Fortu.Infrastructure.MockRepositories.Error;
using Fortu.Mediator;
using Microsoft.AspNetCore.Mvc;

namespace Fortu.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly AccountMessagesFactory _accountMessagesFactory;
        private readonly IMediator _mediator;

        public AccountController(AccountMessagesFactory accountMessagesFactory, IMediator mediator)
        {
            _accountMessagesFactory = accountMessagesFactory;
            _mediator = mediator;
        }

        [HttpGet("token")]
        public Task<IActionResult> Token()
            => _accountMessagesFactory.CreateGetJwtTokenQuery()
                .BindAsync(query => _mediator.Dispatch(query))
                .MapAsync(success => HttpResultFactory.CreateSuccess(success, HttpStatusCode.OK),
                    error =>
                    {
                        switch (error)
                        {
                            case CredentialsErrors.IncorrectPassword _:
                            case AccountNotFoundError _:
                                return HttpResultFactory.CreateFailure<JsonWebTokenDto>(HttpStatusCode.Unauthorized,
                                    "invalid credentials");

                            case AuthorizationHeaderParsingError _:
                            case InvalidAuthorizationHeaderError _:
                                return HttpResultFactory.CreateFailure<JsonWebTokenDto>(HttpStatusCode.BadRequest,
                                    "bad request");

                            case AccountAlreadyAuthenticatedError _:
                                return HttpResultFactory.CreateFailure<JsonWebTokenDto>(HttpStatusCode.Conflict,
                                    "already authenticated");

                            default:
                                return HttpResultFactory.CreateFailure(HttpStatusCode.InternalServerError,
                                    "error while authenticating");
                        }
                    })
                .AsActionResultAsync();

        [HttpPost]
        public Task<IActionResult> Create([FromBody] CreateAccountRequest request)
            => _accountMessagesFactory.CreateCreateAccountCommand(request.Name, request.Password)
                .BindAsync(command => _mediator.Dispatch(command))
                .MapAsync(_ => HttpResultFactory.CreateSuccess(HttpStatusCode.Created),
                    error =>
                    {
                        switch (error)
                        {
                            case ValidationErrors errors:
                                return HttpResultFactory.CreateFailure(HttpStatusCode.BadRequest,
                                    errors.Errors.Select(x => x.Message));

                            case AccountAlreadyExistsError _:
                                return HttpResultFactory.CreateFailure(HttpStatusCode.Conflict,
                                    "account with this name already exists");

                            case AccountAlreadyAuthenticatedError _:
                                return HttpResultFactory.CreateFailure<JsonWebTokenDto>(HttpStatusCode.Conflict,
                                    "already authenticated");

                            default:
                                return HttpResultFactory.CreateFailure(HttpStatusCode.InternalServerError,
                                    "error while adding name");
                        }
                    })
                .AsActionResultAsync();

        [HttpGet]
        public Task<IActionResult> GetAccount() =>
            _accountMessagesFactory.CreateGetAccountQuery()
                .BindAsync(query => _mediator.Dispatch(query))
                .MapAsync(result => HttpResultFactory.CreateSuccess(result, HttpStatusCode.OK),
                    _ => HttpResultFactory.CreateFailure(HttpStatusCode.InternalServerError, "nothing"))
                .AsActionResultAsync();

        [HttpGet("perms")]
        [RequiredPermissions("unavailable")]
        public Task<IActionResult> Test() => Task.FromResult((IActionResult)Ok());
    }
}

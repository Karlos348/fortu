﻿using AutoMapper;
using Fortu.Api.Factories;
using Fortu.Application.Accounts.Extensions;
using Fortu.Application.Users.Services;
using Fortu.Infrastructure.Authorization;
using Fortu.Infrastructure.HttpResult;
using Fortu.Infrastructure.MockRepositories.Extensions;
using Fortu.Infrastructure.Users;
using Fortu.Mediator.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Fortu.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
            => Configuration = configuration;
    
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(x => { x.JsonSerializerOptions.WriteIndented = true; });

            /*services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
                x.MultipartHeadersLengthLimit = int.MaxValue;
                x.MultipartBoundaryLengthLimit = int.MaxValue;
                x.ValueLengthLimit = int.MaxValue;
                x.MemoryBufferThreshold = int.MaxValue;
            });*/
            services.AddSingleton<IAuthorizationHandler, PermissionsAuthorizationHandler>();
            services.AddOptions();
            services.AddHttpContextAccessor();
            services.AddAutoMapper(typeof(Startup));
            services.AddEntityFrameworkSqlServer();

            services.AddMediator();
            services.AddHttpResult();
            services.AddScoped<AccountMessagesFactory>();
            services.AddAuthorization(Configuration);
            services.AddMockRepository();

            services.AddScoped<ILoggedUserProvider, LoggedUserProvider>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(x => x.MapControllers());
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class ValidationErrors : IError
    {
        public ValidationErrors(IEnumerable<IError> errors)
            => Errors = errors;

        public string Message => $"found {Errors.Count()} error(s)";
        public IEnumerable<IError> Errors { get; }
    }
}

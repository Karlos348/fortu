﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class AuthorizationHeaderParsingError : IError
    {
        public string Message => "authorization header parsing error";
    }
}

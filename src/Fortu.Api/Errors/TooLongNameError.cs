﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class TooLongNameError : IError
    {
        public string Message => "name is too long";
    }
}

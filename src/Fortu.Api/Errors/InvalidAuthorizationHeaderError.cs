﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class InvalidAuthorizationHeaderError : IError
    {
        public string Message => "invalid authorization header";
    }
}

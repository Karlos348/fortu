﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class TooShortPassword : IError
    {
        public string Message => "password is too short";
    }
}

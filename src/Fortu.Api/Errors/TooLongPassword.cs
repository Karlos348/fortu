﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class TooLongPassword : IError
    {
        public string Message => "password is too short";
    }
}

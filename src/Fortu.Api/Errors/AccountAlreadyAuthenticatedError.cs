﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class AccountAlreadyAuthenticatedError : IError
    {
        public string Message => "account already authenticated";
    }
}

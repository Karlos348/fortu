﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class NameIsRequiredError : IError
    {
        public string Message => "name is required";
    }
}

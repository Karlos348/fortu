﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class PasswordIsRequired : IError
    {
        public string Message => "password is required";
    }
}

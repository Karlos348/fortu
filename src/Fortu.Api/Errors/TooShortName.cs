﻿using Feree.ResultType.Results;

namespace Fortu.Api.Errors
{
    public class TooShortName : IError
    {
        public string Message => "name is too short";
    }
}
